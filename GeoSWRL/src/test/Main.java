/**
 * 
 */
package test;

import java.io.File;
import java.util.Collection;

import org.apache.log4j.Logger;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLIndividual;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.reasoner.Node;
import org.semanticweb.owlapi.reasoner.NodeSet;
import org.semanticweb.owlapi.search.EntitySearcher;

import br.rc.unesp.geoswrl.GeospatialTesters;
import br.rc.unesp.geoswrl.vocabulary.GeoSPARQLVocab;

import com.clarkparsia.pellet.owlapiv3.PelletReasoner;
import com.clarkparsia.pellet.owlapiv3.PelletReasonerFactory;

/**
 * Main class to test Pellet inference with SWRL rules and geospatial functions 
 * @author rcantonialli
 *
 */
public class Main {
	
	private static final Logger log4j = Logger.getLogger(Main.class);

	/**
	 * @param args
	 * @throws OWLOntologyCreationException 
	 */
	public static void main(String[] args) throws OWLOntologyCreationException {

		File ontologyFile = new File("resources/GeoSWRLValidation.ttl");

		OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
		
		OWLDataFactory dataFactory = manager.getOWLDataFactory();

		OWLOntology ontology = manager.loadOntologyFromOntologyDocument(ontologyFile);

		//Initializing Geospatial Testers
		GeospatialTesters.init();

		//Creating new instance of pellet reasoner.
		PelletReasoner pelletReasoner = PelletReasonerFactory.getInstance().createReasoner(ontology);

		log4j.info("Starting GeoSWRL reasoning...");

		if(!pelletReasoner.isConsistent()){
			String explanation = pelletReasoner.getKB().getExplanation();
			System.out.println(explanation);
			log4j.fatal(explanation);
			System.exit(1);
		}else{
			System.out.println("Ontology is consistent!");
		}
		
		Collection<OWLIndividual> features = EntitySearcher.getIndividuals(GeoSPARQLVocab.CLASS_FEATURE, ontology);
		
		OWLObjectProperty nearby = dataFactory.getOWLObjectProperty(IRI.create("http://www.geonames.org/ontology#nearby"));
		OWLObjectProperty sfIntersects = dataFactory.getOWLObjectProperty(IRI.create("http://www.opengis.net/ont/geosparql#sfIntersects"));
		OWLObjectProperty sfDisjoint = dataFactory.getOWLObjectProperty(IRI.create("http://www.opengis.net/ont/geosparql#sfDisjoint"));

		for (OWLIndividual individual : features) {
			
			NodeSet<OWLNamedIndividual> nearbyFeatures = pelletReasoner.getObjectPropertyValues((OWLNamedIndividual) individual, nearby);
			
			System.out.println(individual + " is nearby: ");
			
			for (Node<OWLNamedIndividual> nbFeature : nearbyFeatures) {
				System.out.println(nbFeature);
			}
			
			System.out.println();
			
			NodeSet<OWLNamedIndividual> disjointFeatures = pelletReasoner.getObjectPropertyValues((OWLNamedIndividual) individual, sfDisjoint);
			
			System.out.println(individual + " is disjoint: ");
			
			for (Node<OWLNamedIndividual> djFeature : disjointFeatures) {
				System.out.println(djFeature);
			}
			
			System.out.println();
			
			
			NodeSet<OWLNamedIndividual> intecsetcsFeatures = pelletReasoner.getObjectPropertyValues((OWLNamedIndividual) individual, sfIntersects);
			
			System.out.println(individual + " intersects: ");
			
			for (Node<OWLNamedIndividual> intFeature : intecsetcsFeatures) {
				System.out.println(intFeature);
			}
			
			System.out.println();
			System.out.println("##############################################################################################################");
			System.out.println();
			
			
		}
		
		
		

	}

}
