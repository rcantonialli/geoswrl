package br.rc.unesp.geoswrl;

import java.util.HashSet;
import java.util.logging.Logger;

import org.geotools.referencing.GeodeticCalculator;
import org.mindswap.pellet.Literal;
import org.mindswap.pellet.utils.ATermUtils;

import com.clarkparsia.pellet.rules.builtins.BuiltInRegistry;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.MultiLineString;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.io.ParseException;
import com.vividsolutions.jts.io.WKTReader;

public class GeospatialTesters {

	private final static Logger log = Logger.getLogger(GeospatialTesters.class.getName());

	/**
	 * The list of all registered functions provided by this class.
	 */
	private static HashSet<GeoSWRLTester> registeredTesters = new HashSet<GeoSWRLTester>();

	/**
	 * @return registeredFuncitions - The list of all registered functions provided by this class.
	 */
	public static HashSet<GeoSWRLTester> getRegisteredFuncitions() {
		return registeredTesters;
	}

	/**
	 * Add a new geospatial operator to this class.
	 * @param geoFunction - New geospatial operator
	 */
	public static void addRegisteredFunction(GeoSWRLTester geoFunction){
		registeredTesters.add(geoFunction);
	}

	/**
	 * Register all registered functions to Pellet {@link BuiltInRegistry}. <br>
	 * This method is also called in {@link GeospatialOperators#init()}
	 */
	public static void registerAll(){

		for(GeoSWRLTester tester : registeredTesters){
			tester.register();
		}
		log.config("Registered all functions");
	}

	/**
	 * Init the Geospatial Operators by creating new instances of geospatial functions.
	 */
	public static void init(){

		registeredTesters.add(Contains.getInstance());
		registeredTesters.add(CoveredBy.getInstance());
		registeredTesters.add(Covers.getInstance());
		registeredTesters.add(Crosses.getInstance());
		registeredTesters.add(Disjoint.getInstance());
		registeredTesters.add(IsWithinDistance.getInstance());
		registeredTesters.add(Intersects.getInstance());
		registeredTesters.add(Overlaps.getInstance());
		registeredTesters.add(Touches.getInstance());
		registeredTesters.add(Within.getInstance());
		registeredTesters.add(AngleBetween.getInstance());

		log.config("Created " + registeredTesters.size() + " default registered functions");

		registerAll();

	}

	/**
	 * &lt;http://neotec.rc.unesp.br/resource/geoswrl/contains&gt;(geometry_1, geometry_2)<br>
	 * <br>
	 * Returns true if the geometry_1 contains the geometry_2.
	 * <br><br>
	 * Where:<br>
	 * <b>geometry_1:</b> a &lt;http://www.opengis.net/ont/geosparql#wktLiteral&gt; geometry representation.<br>
	 * <b>geometry_2:</b> a &lt;http://www.opengis.net/ont/geosparql#wktLiteral&gt; geometry representation.<br>
	 * <br>
	 * This implementation is based on the examples given at http://dior.ics.muni.cz/~makub/owl/#swrl_custom
	 *
	 * @author Rodrigo C. Antonialli 27/02/2015
	 * @version 0.1
	 * @see {@link Geometry#contains(Geometry)}
	 *
	 */
	private static class Contains extends GeoSWRLTester {

		private static Contains instance = new Contains();

		public static Contains getInstance(){
			return instance;
		}

		public Contains() {
			super("contains");
		}

		/* (non-Javadoc)
		 * @see br.rc.unesp.alignment.geoswrl.GeoSWRLTester#test(org.mindswap.pellet.Literal, org.mindswap.pellet.Literal)
		 */
		@Override
		protected boolean test(Literal wkt1, Literal wkt2) {

			String wkt1String = ATermUtils.getLiteralValue(wkt1.getTerm());
			String wkt2String = ATermUtils.getLiteralValue(wkt2.getTerm());

			WKTReader wktReader = new WKTReader();

			try {
				Geometry geom1 = wktReader.read(wkt1String);
				Geometry geom2 = wktReader.read(wkt2String);

				return geom1.contains(geom2);

			} catch (ParseException e) {
				return false;
			}
		}

		@Override
		protected boolean test(Literal literal, Literal literal2, Literal literal3) {
			return false;
		}

	}//Contains

	/**
	 * &lt;http://neotec.rc.unesp.br/resource/geoswrl/coveredBy&gt;(geometry_1, geometry_2)<br>
	 * <br>
	 * Returns true if geometry_1 is covered by geometry_2.
	 * <br><br>
	 * Where:<br>
	 * <b>geometry_1:</b> a &lt;http://www.opengis.net/ont/geosparql#wktLiteral&gt; geometry representation.<br>
	 * <b>geometry_2:</b> a &lt;http://www.opengis.net/ont/geosparql#wktLiteral&gt; geometry representation.<br>
	 * <br>
	 * This implementation is based on the examples given at http://dior.ics.muni.cz/~makub/owl/#swrl_custom
	 *
	 * @author Rodrigo C. Antonialli 27/02/2015
	 * @version 0.1
	 * @see {@link Geometry#coveredBy(Geometry)}
	 * @see {@link Geometry#covers(Geometry)}
	 *
	 */
	private static class CoveredBy extends GeoSWRLTester {

		private static CoveredBy instance = new CoveredBy();

		public static CoveredBy getInstance(){
			return instance;
		}

		public CoveredBy() {
			super("coveredBy");
		}

		@Override
		protected boolean test(Literal wkt1, Literal wkt2) {
			String wkt1String = ATermUtils.getLiteralValue(wkt1.getTerm());
			String wkt2String = ATermUtils.getLiteralValue(wkt2.getTerm());

			WKTReader wktReader = new WKTReader();

			try {
				Geometry geom1 = wktReader.read(wkt1String);
				Geometry geom2 = wktReader.read(wkt2String);

				return geom1.coveredBy(geom2);

			} catch (ParseException e) {
				return false;
			}
		}

		@Override
		protected boolean test(Literal literal, Literal literal2, Literal literal3) {
			return false;
		}




	}//CoveredBy

	/**
	 * &lt;http://neotec.rc.unesp.br/resource/geoswrl/covers&gt;(geometry_1, geometry_2)<br>
	 * <br>
	 * Returns true if geometry_1 covers geometry_2.
	 * <br><br>
	 * Where:<br>
	 * <b>geometry_1:</b> a &lt;http://www.opengis.net/ont/geosparql#wktLiteral&gt; geometry representation.<br>
	 * <b>geometry_2:</b> a &lt;http://www.opengis.net/ont/geosparql#wktLiteral&gt; geometry representation.<br>
	 * <br>
	 * This implementation is based on the examples given at http://dior.ics.muni.cz/~makub/owl/#swrl_custom
	 *
	 * @author Rodrigo C. Antonialli 27/02/2015
	 * @version 0.1
	 * @see {@link Geometry#covers(Geometry)}
	 * @see {@link Geometry#coveredBy(Geometry)}
	 *
	 */
	private static class Covers extends GeoSWRLTester {

		private static Covers instance = new Covers();

		public static Covers getInstance(){
			return instance;
		}

		public Covers() {
			super("covers");
		}

		@Override
		protected boolean test(Literal wkt1, Literal wkt2) {
			String wkt1String = ATermUtils.getLiteralValue(wkt1.getTerm());
			String wkt2String = ATermUtils.getLiteralValue(wkt2.getTerm());

			WKTReader wktReader = new WKTReader();

			try {
				Geometry geom1 = wktReader.read(wkt1String);
				Geometry geom2 = wktReader.read(wkt2String);

				return geom1.covers(geom2);

			} catch (ParseException e) {
				return false;
			}
		}

		@Override
		protected boolean test(Literal literal, Literal literal2, Literal literal3) {
			return false;
		}

	}//Covers

	/**
	 * &lt;http://neotec.rc.unesp.br/resource/geoswrl/crosses&gt;(geometry_1, geometry_2)<br>
	 * <br>
	 * Returns true if geometry_1 crosses geometry_2.
	 * <br><br>
	 * Where:<br>
	 * <b>geometry_1:</b> a &lt;http://www.opengis.net/ont/geosparql#wktLiteral&gt; geometry representation.<br>
	 * <b>geometry_2:</b> a &lt;http://www.opengis.net/ont/geosparql#wktLiteral&gt; geometry representation.<br>
	 * <br>
	 * This implementation is based on the examples given at http://dior.ics.muni.cz/~makub/owl/#swrl_custom
	 *
	 * @author Rodrigo C. Antonialli 27/02/2015
	 * @version 0.1
	 * @see {@link Geometry#crosses(Geometry)}
	 *
	 */
	private static class Crosses extends GeoSWRLTester {

		private static Crosses instance = new Crosses();

		public static Crosses getInstance(){
			return instance;
		}

		/**
		 * @param fname
		 */
		public Crosses() {
			super("crosses");
		}

		@Override
		protected boolean test(Literal wkt1, Literal wkt2) {
			String wkt1String = ATermUtils.getLiteralValue(wkt1.getTerm());
			String wkt2String = ATermUtils.getLiteralValue(wkt2.getTerm());

			WKTReader wktReader = new WKTReader();

			try {
				Geometry geom1 = wktReader.read(wkt1String);
				Geometry geom2 = wktReader.read(wkt2String);

				return geom1.crosses(geom2);

			} catch (ParseException e) {
				return false;
			}
		}

		@Override
		protected boolean test(Literal literal, Literal literal2, Literal literal3) {
			return false;
		}

	}//Crosses

	/**
	 * &lt;http://neotec.rc.unesp.br/resource/geoswrl/disjoint&gt;(geometry_1, geometry_2)<br>
	 * <br>
	 * Returns true if geometry_1 and geometry_2 are disjoint, i.e. "not" intersects.
	 * <br><br>
	 * Where:<br>
	 * <b>geometry_1:</b> a &lt;http://www.opengis.net/ont/geosparql#wktLiteral&gt; geometry representation.<br>
	 * <b>geometry_2:</b> a &lt;http://www.opengis.net/ont/geosparql#wktLiteral&gt; geometry representation.<br>
	 * <br>
	 * This implementation is based on the examples given at http://dior.ics.muni.cz/~makub/owl/#swrl_custom
	 *
	 * @author Rodrigo C. Antonialli 27/02/2015
	 * @version 0.1
	 * @see {@link Geometry#disjoint(Geometry)}
	 *
	 */
	private static class Disjoint extends GeoSWRLTester {

		private static Disjoint instance = new Disjoint();

		public static Disjoint getInstance(){
			return instance;
		}

		/**
		 * @param fname
		 */
		public Disjoint() {
			super("disjoint");
		}

		@Override
		protected boolean test(Literal wkt1, Literal wkt2) {
			String wkt1String = ATermUtils.getLiteralValue(wkt1.getTerm());
			String wkt2String = ATermUtils.getLiteralValue(wkt2.getTerm());

			WKTReader wktReader = new WKTReader();

			try {
				Geometry geom1 = wktReader.read(wkt1String);
				Geometry geom2 = wktReader.read(wkt2String);

				return geom1.disjoint(geom2);

			} catch (ParseException e) {
				return false;
			}
		}

		@Override
		protected boolean test(Literal literal, Literal literal2, Literal literal3) {
			return false;
		}
	}//Disjoint

	/**
	 * &lt;http://neotec.rc.unesp.br/resource/geoswrl/isWithinDistance&gt;(geometry_1, geometry_2, distanceLimit)<br>
	 * <br>
	 * Calculate the relative distance between two geometries and states if they are near each other, within a given limit.
	 * <br>
	 * Returns true if geometries are within the specified distance of one another.
	 * <br><br>
	 * Where:<br>
	 * <b>geometry_1:</b> a &lt;http://www.opengis.net/ont/geosparql#wktLiteral&gt; geometry representation.<br>
	 * <b>geometry_2:</b> a &lt;http://www.opengis.net/ont/geosparql#wktLiteral&gt; geometry representation.<br>
	 * <b>distanceLimit:</b> the distance limit to consider the distance between the two geometries. <br>
	 * <br>
	 * This implementation is based on the examples given at http://dior.ics.muni.cz/~makub/owl/#swrl_custom
	 *
	 * @author Rodrigo C. Antonialli 25/02/2015
	 * @version 0.1
	 * @see {@link Geometry#isWithinDistance(Geometry, double)}
	 */
	private static class IsWithinDistance extends GeoSWRLTester {

		private static IsWithinDistance instance = new IsWithinDistance();

		public static IsWithinDistance getInstance(){
			return instance;
		}

		public IsWithinDistance() {
			super("isWithinDistance");
		}

		/* (non-Javadoc)
		 * @see br.rc.unesp.alignment.geoswrl.GeoSWRLTester#test(org.mindswap.pellet.Literal, org.mindswap.pellet.Literal, org.mindswap.pellet.Literal)
		 */
		@Override
		protected boolean test(Literal wkt1, Literal wkt2, Literal threshold) {

			double distanceLimit = Double.parseDouble(threshold.getLexicalValue());

			String wkt1String = ATermUtils.getLiteralValue(wkt1.getTerm());
			String wkt2String = ATermUtils.getLiteralValue(wkt2.getTerm());

			WKTReader wktReader = new WKTReader();

			try {
				Geometry geom1 = wktReader.read(wkt1String);
				Geometry geom2 = wktReader.read(wkt2String);

				return geom1.isWithinDistance(geom2, distanceLimit);

			} catch (ParseException e) {
				e.printStackTrace();
				return false;
			}
		}

		/* (non-Javadoc)
		 * @see br.rc.unesp.alignment.geoswrl.GeoSWRLTester#test(org.mindswap.pellet.Literal, org.mindswap.pellet.Literal)
		 */
		@Override
		protected boolean test(Literal literal, Literal literal2) {
			return false;
		}

	}//IsWithinDistance

	/**
	 * &lt;http://neotec.rc.unesp.br/resource/geoswrl/intersects&gt;(geometry_1, geometry_2)<br>
	 * <br>
	 * Returns true if geometry_1 intersects geometry_2.
	 * <br><br>
	 * Where:<br>
	 * <b>geometry_1:</b> a &lt;http://www.opengis.net/ont/geosparql#wktLiteral&gt; geometry representation.<br>
	 * <b>geometry_2:</b> a &lt;http://www.opengis.net/ont/geosparql#wktLiteral&gt; geometry representation.<br>
	 * <br>
	 * This implementation is based on the examples given at http://dior.ics.muni.cz/~makub/owl/#swrl_custom
	 *
	 * @author Rodrigo C. Antonialli 27/02/2015
	 * @version 0.1
	 * @see {@link Geometry#intersects(Geometry)}
	 *
	 */
	private static class Intersects extends GeoSWRLTester {

		private static Intersects instance = new Intersects();

		public static Intersects getInstance(){
			return instance;
		}

		public Intersects() {
			super("intersects");
		}

		@Override
		protected boolean test(Literal wkt1, Literal wkt2) {
			String wkt1String = ATermUtils.getLiteralValue(wkt1.getTerm());
			String wkt2String = ATermUtils.getLiteralValue(wkt2.getTerm());

			WKTReader wktReader = new WKTReader();

			try {
				Geometry geom1 = wktReader.read(wkt1String);
				Geometry geom2 = wktReader.read(wkt2String);

				return geom1.intersects(geom2);

			} catch (ParseException e) {
				return false;
			}
		}

		@Override
		protected boolean test(Literal literal, Literal literal2, Literal literal3) {
			return false;
		}

	}//Intersects

	/**
	 * &lt;http://neotec.rc.unesp.br/resource/geoswrl/overlaps&gt;(geometry_1, geometry_2)<br>
	 * <br>
	 * Returns true if geometry_1 overlaps geometry_2. It has to actually overlap the edge, being within or touching will not work
	 * <br><br>
	 * Where:<br>
	 * <b>geometry_1:</b> a &lt;http://www.opengis.net/ont/geosparql#wktLiteral&gt; geometry representation.<br>
	 * <b>geometry_2:</b> a &lt;http://www.opengis.net/ont/geosparql#wktLiteral&gt; geometry representation.<br>
	 * <br>
	 * This implementation is based on the examples given at http://dior.ics.muni.cz/~makub/owl/#swrl_custom
	 *
	 * @author Rodrigo C. Antonialli 27/02/2015
	 * @version 0.1
	 * @see {@link Geometry#overlaps(Geometry)}
	 *
	 */
	private static class Overlaps extends GeoSWRLTester {

		private static Overlaps instance = new Overlaps();

		public static Overlaps getInstance(){
			return instance;
		}

		public Overlaps() {
			super("overlaps");
		}

		@Override
		protected boolean test(Literal wkt1, Literal wkt2) {

			String wkt1String = ATermUtils.getLiteralValue(wkt1.getTerm());
			String wkt2String = ATermUtils.getLiteralValue(wkt2.getTerm());

			WKTReader wktReader = new WKTReader();

			try {
				Geometry geom1 = wktReader.read(wkt1String);
				Geometry geom2 = wktReader.read(wkt2String);

				return geom1.overlaps(geom2);

			} catch (ParseException e) {
				return false;
			}
		}

		@Override
		protected boolean test(Literal literal, Literal literal2, Literal literal3) {
			return false;
		}

	}//Overlaps

	/**
	 * &lt;http://neotec.rc.unesp.br/resource/geoswrl/touches&gt;(geometry_1, geometry_2)<br>
	 * <br>
	 * Returns true if geometry_1 touches geometry_2. Crossing or overlap will not work.
	 * <br><br>
	 * Where:<br>
	 * <b>geometry_1:</b> a &lt;http://www.opengis.net/ont/geosparql#wktLiteral&gt; geometry representation.<br>
	 * <b>geometry_2:</b> a &lt;http://www.opengis.net/ont/geosparql#wktLiteral&gt; geometry representation.<br>
	 * <br>
	 * This implementation is based on the examples given at http://dior.ics.muni.cz/~makub/owl/#swrl_custom
	 *
	 * @author Rodrigo C. Antonialli 27/02/2015
	 * @version 0.1
	 * @see {@link Geometry#touches(Geometry)}
	 *
	 */
	private static class Touches extends GeoSWRLTester{

		private static Touches instance = new Touches();

		public static Touches getInstance(){
			return instance;
		}

		public Touches() {
			super("touches");
		}

		@Override
		protected boolean test(Literal wkt1, Literal wkt2) {
			String wkt1String = ATermUtils.getLiteralValue(wkt1.getTerm());
			String wkt2String = ATermUtils.getLiteralValue(wkt2.getTerm());

			WKTReader wktReader = new WKTReader();

			try {
				Geometry geom1 = wktReader.read(wkt1String);
				Geometry geom2 = wktReader.read(wkt2String);

				return geom1.touches(geom2);

			} catch (ParseException e) {
				return false;
			}
		}

		@Override
		protected boolean test(Literal literal, Literal literal2, Literal literal3) {
			return false;
		}

	}//Touches

	/**
	 * &lt;http://neotec.rc.unesp.br/resource/geoswrl/within&gt;(geometry_1, geometry_2)<br>
	 * <br>
	 * Returns true if geometry_1 is full inside geometry_2.
	 * <br><br>
	 * Where:<br>
	 * <b>geometry_1:</b> a &lt;http://www.opengis.net/ont/geosparql#wktLiteral&gt; geometry representation.<br>
	 * <b>geometry_2:</b> a &lt;http://www.opengis.net/ont/geosparql#wktLiteral&gt; geometry representation.<br>
	 * <br>
	 * This implementation is based on the examples given at http://dior.ics.muni.cz/~makub/owl/#swrl_custom
	 *
	 * @author Rodrigo C. Antonialli 27/02/2015
	 * @version 0.1
	 * @see {@link Geometry#within(Geometry)}
	 *
	 */
	private static class Within extends GeoSWRLTester {

		private static Within instance = new Within();

		public static Within getInstance(){
			return instance;
		}

		public Within() {
			super("within");
		}

		@Override
		protected boolean test(Literal wkt1, Literal wkt2) {
			String wkt1String = ATermUtils.getLiteralValue(wkt1.getTerm());
			String wkt2String = ATermUtils.getLiteralValue(wkt2.getTerm());

			WKTReader wktReader = new WKTReader();

			try {
				Geometry geom1 = wktReader.read(wkt1String);
				Geometry geom2 = wktReader.read(wkt2String);

				return geom1.within(geom2);

			} catch (ParseException e) {
				return false;
			}
		}

		@Override
		protected boolean test(Literal literal, Literal literal2, Literal literal3) {
			return false;
		}

	}//Within

	/**
	 * &lt;http://neotec.rc.unesp.br/resource/geoswrl/angleBetween&gt;(geometry_1, geometry_2, limit)<br>
	 * <br>
	 * Returns true if the angle between geometry_1 and geometry_2 is smaller or equal to limit.
	 * <br><br>
	 * Where:<br>
	 * <b>geometry_1:</b> a &lt;http://www.opengis.net/ont/geosparql#wktLiteral&gt; geometry representation.<br>
	 * <b>geometry_2:</b> a &lt;http://www.opengis.net/ont/geosparql#wktLiteral&gt; geometry representation.<br>
	 * <b>limit:</b> the angle limit to consider the distance between the two geometries.<br>
	 * <br>
	 *
	 * @author Rodrigo C. Antonialli 27/02/2015
	 * @version 0.1
	 *
	 */
	private static class AngleBetween extends GeoSWRLTester {

		private static AngleBetween instance = new AngleBetween();

		public static AngleBetween getInstance(){
			return instance;
		}

		public AngleBetween() {
			super("angleBetween");
		}

		@Override
		protected boolean test(Literal wkt1, Literal wkt2, Literal threshold) {

			String wkt1String = ATermUtils.getLiteralValue(wkt1.getTerm());
			String wkt2String = ATermUtils.getLiteralValue(wkt2.getTerm());

			double limit = Double.parseDouble(threshold.getLexicalValue());

			WKTReader wktReader = new WKTReader();

			try {

				Geometry geom1 = wktReader.read(wkt1String);
				Geometry geom2 = wktReader.read(wkt2String);

				GeodeticCalculator gc = new GeodeticCalculator();

				double azimuth1 = 0.0;
				double azimuth2 = 0.0;

				if(geom1 instanceof MultiLineString){
					MultiLineString multiline1 = (MultiLineString) geom1;
					Point startPoint1 = ((LineString)multiline1.getGeometryN(0)).getStartPoint();
					Point endPoint1 = ((LineString)multiline1.getGeometryN(multiline1.getNumGeometries()-1)).getEndPoint();
					gc.setStartingGeographicPoint(startPoint1.getX(), startPoint1.getY());
					gc.setDestinationGeographicPoint(endPoint1.getX(), endPoint1.getY());
					azimuth1 =  gc.getAzimuth();
				}else if(geom1 instanceof LineString){
					Point startPoint1 = ((LineString) geom1).getStartPoint();
					Point endPoint1 = ((LineString) geom1).getEndPoint();
					gc.setStartingGeographicPoint(startPoint1.getX(), startPoint1.getY());
					gc.setDestinationGeographicPoint(endPoint1.getX(), endPoint1.getY());
					azimuth1 =  gc.getAzimuth();
				}else{
					return false;
				}

				if(azimuth1 < 0){
					azimuth1 = 180.0 + azimuth1;
				}

				if(geom2 instanceof MultiLineString){
					MultiLineString multiline1 = (MultiLineString) geom2;
					Point startPoint1 = ((LineString)multiline1.getGeometryN(0)).getStartPoint();
					Point endPoint1 = ((LineString)multiline1.getGeometryN(multiline1.getNumGeometries()-1)).getEndPoint();
					gc.setStartingGeographicPoint(startPoint1.getX(), startPoint1.getY());
					gc.setDestinationGeographicPoint(endPoint1.getX(), endPoint1.getY());
					azimuth2 =  gc.getAzimuth();
				}else if(geom2 instanceof LineString){
					Point startPoint1 = ((LineString) geom2).getStartPoint();
					Point endPoint1 = ((LineString) geom2).getEndPoint();
					gc.setStartingGeographicPoint(startPoint1.getX(), startPoint1.getY());
					gc.setDestinationGeographicPoint(endPoint1.getX(), endPoint1.getY());
					azimuth2 =  gc.getAzimuth();
				}else{
					return false;
				}

				if(azimuth2 < 0){
					azimuth2 = 180.0 + azimuth2;
				}

				double angleBetween;

				if(azimuth1 > azimuth2){
					angleBetween = Math.round((azimuth1 - azimuth2));
				}else{
					angleBetween = Math.round((azimuth2 - azimuth1));
				}

				return (angleBetween <= limit);

			} catch (ParseException e) {
				return false;
			}
		}

		@Override
		protected boolean test(Literal literal, Literal literal2) {
			return false;
		}

	}//AngleBetween



}
