/**
 *
 */
package br.rc.unesp.geoswrl;

import org.apache.log4j.Logger;
import org.mindswap.pellet.Literal;
import org.mindswap.pellet.utils.ATermUtils;

import br.rc.unesp.geoswrl.vocabulary.GeoSPARQLVocab;

import com.clarkparsia.pellet.rules.builtins.BuiltInRegistry;
import com.clarkparsia.pellet.rules.builtins.NoSuchBuiltIn;
import com.clarkparsia.pellet.rules.builtins.Tester;
import com.clarkparsia.pellet.rules.builtins.TesterBuiltIn;

/**
 * @author Rodrigo C. Antonialli 16/11/2015 11:49:47
 *
 */
public abstract class GeoSWRLTester implements Tester {

	private final static Logger log = Logger.getLogger(GeoSWRLTester.class.getName());

	private final String prefix = "geoswrl";
	private final String namespace = "http://neotec.rc.unesp.br/resource/geoswrl/";
	private final String name;
	private final String uri;
	private final String prefixedName;

	/**
	 * @param fname
	 */
	public GeoSWRLTester(String fname) {
		this.name = fname;
		this.uri = this.namespace.concat(this.name);
		this.prefixedName = this.prefix.concat(":").concat(this.name);
	}

	/**
	 * Every builtIn should register itself into Pellet {@link BuiltInRegistry}.
	 */
	public void register(){
		if(BuiltInRegistry.instance.getBuiltIn(this.getURI()) instanceof NoSuchBuiltIn){
			log.debug("Registering " + this.getPrefixedName());
			BuiltInRegistry.instance.registerBuiltIn(this.getURI(), new TesterBuiltIn(this));
		}else{
			log.debug(this.getPrefixedName() + " already registered");
		}
	}

	/**
	 * @return prefix - The prefix associated to GeoSWRL URI.
	 */
	public String getPrefix() {
		return prefix;
	}


	/**
	 * @return namespace - The GeoSWRL URI.
	 */
	public String getNamespace() {
		return namespace;
	}


	/**
	 * @return name - The function name.
	 */
	public String getName() {
		return name;
	}


	/**
	 * @return uri - The BuiltIn URI.
	 */
	public String getURI() {
		return uri;
	}


	/**
	 * @return prefixedName - The prefixed name of the built in.
	 */
	public String getPrefixedName() {
		return prefixedName;
	}

	/**
	 * Check if a given literal has the {@link GeoSPARQL#WKT_LITERAL} datatype.
	 * @param argument - The literal to be checked.
	 * @return <b>true</b> if the literal datatype is {@link GeoSPARQL#WKT_LITERAL}. <br><b>false</b> if literal has any other datatype.
	 */
	public boolean checkGeometryParam(Literal argument){

		if(!argument.isLiteral()){
			return false;
		}

		if(argument.getTerm() == null){
			return false;
		}

		String geometryType = ATermUtils.getLiteralDatatype(argument.getTerm());

		if(!geometryType.equals(GeoSPARQLVocab.DATATYPE_WKTLITERAL.getIRI().toString())){
			return false;
		}else{
			return true;
		}


	}


	/* (non-Javadoc)
	 * @see com.clarkparsia.pellet.rules.builtins.Tester#test(org.mindswap.pellet.Literal[])
	 */
	public boolean test(Literal[] args) {
		if(args.length == 2){
			if(checkGeometryParam(args[0]) && checkGeometryParam(args[1])){
				return test(args[0], args[1]);
			}else{
				return false;
			}
		}else if(args.length == 3){
			if(checkGeometryParam(args[0]) && checkGeometryParam(args[1])){
				return test(args[0],args[1],args[2]);
			}else{
				return false;
			}
		}
		return false;
	}

	/**
	 * @param literal
	 * @param literal2
	 * @param literal3
	 * @return
	 */
	protected abstract boolean test(Literal literal, Literal literal2, Literal literal3);

	/**
	 * @param literal
	 * @param literal2
	 * @return
	 */
	protected abstract boolean test(Literal literal, Literal literal2);




}
