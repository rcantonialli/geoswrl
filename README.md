# GeoSWRL Builtins #

The GeoSWRL Built-in provides custom SWRL builtins implemented within Pellet reasoner. The project repository is in a very early stage of development. The project contains an example ontology with some features and geometries generated using [QGIS](http://www.qgis.org/en/site/). The features were drawn in QGIS and their geometry were copied into instances asWKT properties.

The sample ontology was built in [Protege](http://protege.stanford.edu/) desktop. To use a custom function in protege swrl tab, the full URI, between *<>* are necessary, as in the example bellow:


```
#!SWRL

Feature(?f1), Feature(?f2), hasGeometry(?f1, ?g1), hasGeometry(?f2, ?g2), asWKT(?g1, ?wkt1), asWKT(?g2, ?wkt2), <http://neotec.rc.unesp.br/resource/geoswrl/intersects>(?wkt1, ?wkt2), DifferentFrom(?f1, ?f2) -> sfIntersects(?f1,?f2) 
```

### Notes about the implementation: ###

* The GeoSWRL builtins are built as custom functions for [Pellet reasoner by Ignazio Palmisano](https://github.com/ignazio1977/pellet).
* It's necessary the features and geometries to be described by the [GEOSPARQL vocabulary](http://schemas.opengis.net/geosparql/1.0/geosparql_vocab_all.rdf).

### How do I get set up? ###

* The project was built in Eclipse, as a maven project. Once you clone the repository, you can import the project into eclipse.

### Who do I talk to? ###

* Rodrigo C. Antonialli - Repository owner